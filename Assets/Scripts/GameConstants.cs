﻿public class GameConstants{
    public static float WaitingAttention = 15f;
    public static float WaitingDispatch = 30f;
    public static float ClientSpawmingDelay = 7.5f;
    public static float GestureRemainingTime = 1f;
    public static float MachineProducingTime = 10f;
    public static int MaxProductsPerOrder = 2;
    public static float GridSizeX = 4;
    public static float GridSizeY = 3;
    public static float IsometricTileHalfWidth = 2;
    public static float IsometricTileHalfHeight = 1;
    public static float AdminVelocity = 2f;
}

public enum ProductType { Soda, PopCorn, End };//End siempre será el último producto en la lista y determina la cantidad de posibles productos
public enum MachineState { Idle, Working, ProductReady };
public enum ClientState { Idle, WaitingProducts, Done, Retreat };
public enum AdminState { Idle, Moving, ReachingDestiny };
public enum GestureType { Good, Bad, Money, Check, Cancel, Time }
public enum ButtonType { TakeOrder, OrderTaked}