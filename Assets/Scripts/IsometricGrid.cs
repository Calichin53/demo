﻿using System.Collections.Generic;
using UnityEngine;

public class IsometricGrid {

    public Node[,] nodesInGrid;
    public float gridXLenght, gridYLenght;

    public IsometricGrid(int sizeX, int sizeY)
    {
        nodesInGrid = new Node[sizeX, sizeY];
        gridXLenght = sizeX;
        gridYLenght = sizeY;
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                nodesInGrid[x, y] = new Node(true, new Vector2(x, y));
            }
        }
    }

    public void SetNotWalkable(int x, int y )
    {
        nodesInGrid[x,y].Walkable = false;
    }

    public List<Node> GetNeighbours(Node node)
    {
        int validX, validY;
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0) continue;

                validX = node.isoX + x;
                validY = node.isoY + y;
                if (validX >= 0 && validX < gridXLenght && validY >= 0 && validY < gridYLenght)
                {
                    neighbours.Add(nodesInGrid[validX, validY]);
                }                               
            }
        }
        return neighbours;
    }

    public Vector2 IsometricToWorldPosition(Node node)
    {
        return IsometricToWorldPosition(node.isoX, node.isoY);
    }

    public Vector2 IsometricToWorldPosition(Vector2 position)
    {
        return IsometricToWorldPosition((int)position.x, (int)position.y);
    }

    public Vector2 IsometricToWorldPosition(int x, int y)
    {
        return new Vector2((x-y)*GameConstants.IsometricTileHalfWidth, ((gridYLenght+1)/2 -x-y)*GameConstants.IsometricTileHalfHeight);
    }

    public Node GetNodeAtIsometric(Vector2 position)
    {
        if (position.x < gridXLenght && position.y < gridYLenght)
        {
            return nodesInGrid[(int)position.x, (int)position.y];
        }
            return null;
    }
}