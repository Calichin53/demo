﻿using UnityEngine;

public class Machine : MonoBehaviour, IAction {

    public Vector2 InteractionPosition;
    public ProductType Product;
    public float ProducingDelay = GameConstants.MachineProducingTime;
    public SpriteRenderer ProductImage;

    float mCurrentProducingTime =0;
    MachineState mState = MachineState.Idle;
	
	void Update () {
        switch (mState)
        {
            case MachineState.Idle:
            case MachineState.ProductReady:
                break;
            case MachineState.Working:
                mCurrentProducingTime += Time.deltaTime;
                if (mCurrentProducingTime >= ProducingDelay)
                {
                    mCurrentProducingTime = 0;
                    GameManager.GM.DropGesture(GestureType.Check, this.transform.position);
                    mState = MachineState.ProductReady;
                    ProductImage.sprite = GameManager.GM.GetProductSprite(Product);
                }
                break;
        }
	}

    public void Interact()
    {
        switch (mState)
        {
            case MachineState.Idle:                //La máquina empieza a producir
                mState = MachineState.Working;
                GameManager.GM.DropGesture(GestureType.Time, this.transform.position);
                break;
            case MachineState.ProductReady:                //Entregar producto al Admin si se puede, en caso contrario no hace nada
                if (GameManager.GM.AdminPicksProduct(Product))
                {//Si se pudo, vuelve al estado de espera
                    mState = MachineState.Idle;
                    ProductImage.sprite = null;
                }
                break;
            case MachineState.Working:
                break;
        }
    }

    private void OnMouseDown()
    {
        GameManager.GM.AddAction(InteractionPosition, this);
    }
}