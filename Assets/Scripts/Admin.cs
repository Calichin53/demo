﻿using UnityEngine;

public class Admin : MonoBehaviour {

    public SpriteRenderer productImage;
    public Vector2 isometricPosition;
    public Vector2 Position { get { return mPosition; } set { mPosition = value; } }
    public ProductType ProductInHand;
    AdminState mState=AdminState.Idle;
    Vector2 mPosition, mDirection, mDestiny;
    float mVelocity;

	void Awake () {
        ProductInHand = ProductType.End; //End representa la mano vacía, NO TIENE PRODUCTOS
        mPosition = mDestiny= transform.position;
        mDirection = Vector2.zero;
        mVelocity = GameConstants.AdminVelocity;
	}
	
	void Update () {
        switch (mState)
        {
            case AdminState.Idle://Se prepara la siguiente acción del stack en caso hubiera, en caso contrario no hace nada
                GameManager.GM.CalculateNextActionPath();
                break;
            case AdminState.Moving://Actualiza su posición en función de la ruta por la que se está moviendo
                mPosition = (Vector2)transform.position + mDirection *mVelocity* Time.deltaTime;
                if ((mDestiny - mPosition).SqrMagnitude() <= 0.01f)
                {
                    transform.position = mDestiny;
                    mPosition = mDestiny;
                    mState = AdminState.ReachingDestiny;
                }
                else
                {
                    transform.position = mPosition;
                }
                break;
            case AdminState.ReachingDestiny://Ejecutar acción correspondiente en esa posición
                GameManager.GM.SetNewAdminDestiny();
                break;
        }
	}

    public bool TryToPickProduct(ProductType product)
    {
        if (ProductInHand == ProductType.End)
        {//Manos libres, puede coger el producto
            ProductInHand = product;
            return true;
        }//Manos ocupadas
        return false;
    }

    public void SetNewDestiny(Vector2 position)
    {
        mDirection = (position - mPosition).normalized;
        mDestiny = position;
        mState = AdminState.Moving;
    }

    public void Interact()
    {
        GameManager.GM.ExecutesNextAction();
        RefreshHand();
        mState = AdminState.Idle;
    }

    void RefreshHand()
    {

        if (ProductInHand != ProductType.End)
        {
            productImage.sprite = GameManager.GM.GetProductSprite(ProductInHand);
        }
        else
        {
            productImage.sprite = null;
        }

    }
}