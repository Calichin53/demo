﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gesture : MonoBehaviour {
    public bool ZoomIn, MoveTop, FadeOut;
    public float GestureTime = GameConstants.GestureRemainingTime;
    float mCurrentTime, factor;
    SpriteRenderer Graph;
	// Use this for initialization
	void Awake () {
        mCurrentTime = factor = 0;
        Graph = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        mCurrentTime += Time.deltaTime;
        factor = mCurrentTime / GestureTime;
        if (ZoomIn)
        { transform.localScale = Vector3.one * (0.5f + factor); }
        if (MoveTop)
        { transform.Translate(Vector3.up * 0.1f); }
        if (FadeOut)
        { Graph.color = new Color(1, 1, 1, 1-factor); }

        if (factor >= 1f)
        { Destroy(this.gameObject); }
	}
}
