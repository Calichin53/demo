﻿using System.Collections.Generic;
using UnityEngine;

public class Client : MonoBehaviour, IAction {

    public GameObject mTray, mBody;
    public SpriteRenderer ProductA, ProductB, Button;
    public Vector2 InteractionPosition;
    public Transform BarFG;
    float mWaitingForAttentionTime, mWaitingForDespatchingTime, mCurrentWaitingTime;
    List<ProductType> mProductsOrdered;
    ClientState mState;

	void Awake () {
        mProductsOrdered = new List<ProductType>();
        mCurrentWaitingTime = 0;
        mWaitingForAttentionTime = GameConstants.WaitingAttention;
        mWaitingForDespatchingTime = GameConstants.WaitingDispatch;
        mState = ClientState.Idle;
	}

    void Update () {
        switch (mState)
        {
            case ClientState.Idle:
                mCurrentWaitingTime += Time.deltaTime;
                UpdateBar(mCurrentWaitingTime / mWaitingForAttentionTime);
                if (mCurrentWaitingTime >= mWaitingForAttentionTime)
                {
                    mCurrentWaitingTime = 0;
                    mState = ClientState.Done;
                }
                break;
            case ClientState.WaitingProducts:
                mCurrentWaitingTime += Time.deltaTime;
                UpdateBar(mCurrentWaitingTime / mWaitingForDespatchingTime);
                if (mCurrentWaitingTime >= mWaitingForDespatchingTime)
                {
                    mCurrentWaitingTime = 0;
                    mState = ClientState.Done;
                }
                    break;
            case ClientState.Done:
                GameManager.GM.DropGesture(GestureType.Bad, mBody.transform.position);
                mState = ClientState.Retreat;
                break;
            case ClientState.Retreat:
                HideAll();
                break;
        }
    }

    bool IsOrderCompleted()
    {
        if (mProductsOrdered.Count <= 0)
            return true;
        else
            return false;
    }

    public bool TryReceiveProduct(ProductType product)
    {
        if (mProductsOrdered.Contains(product))
        {   //Si el producto está en la lista lo recibe (es decir, lo quita de la lista) y retorno verdadero
            mProductsOrdered.Remove(product);
            RefreshTray();
            return true;
        }
        else
            return false;
    }

    public void ResetClient()
    {//reinicia contadores, reinicia productos, muestra el cliente
        mProductsOrdered = GameManager.GM.GenerateNewOrder(GameConstants.MaxProductsPerOrder);
        mWaitingForAttentionTime = GameConstants.WaitingAttention;
        mWaitingForDespatchingTime = GameConstants.WaitingDispatch;
        mCurrentWaitingTime = 0;
    }

    public void ShowClient()
    {
        Button.sprite = GameManager.GM.GetButtonSprite(ButtonType.TakeOrder);
        mBody.SetActive(true);
        mState = ClientState.Idle;
    }

    public void ShowTray()
    {
        Button.sprite = GameManager.GM.GetButtonSprite(ButtonType.OrderTaked);
        RefreshTray();
        mTray.SetActive(true);}

    public void HideAll()
    {
        mBody.SetActive(false);
        mTray.SetActive(false);
        mState = ClientState.Retreat;
    }

    public void Interact()
    {
        switch (mState)
        {
            case ClientState.Idle:
                ShowTray();
                mState = ClientState.WaitingProducts;
                mCurrentWaitingTime = 0;
                break;
            case ClientState.WaitingProducts://Si tiene uno de los productos lo acepta.
                if (GameManager.GM.AdminDispatchProductTo(this))
                {//Gesto positivo
                    GameManager.GM.DropGesture(GestureType.Good, this.transform.position);
                    if (IsOrderCompleted())
                    {
                        GameManager.GM.DropGesture(GestureType.Money, GameManager.GM.admin.transform.position);
                        mState = ClientState.Retreat;
                    }
                }
                else
                {//Gesto negativo
                    GameManager.GM.DropGesture(GestureType.Bad, this.transform.position);
                }
                break;
            case ClientState.Done:
            case ClientState.Retreat:
                break;
        }
    }

    private void OnMouseDown()
    {
        GameManager.GM.AddAction(InteractionPosition, this);
    }

    void UpdateBar(float val)
    {
        BarFG.transform.localScale= new Vector3(1-val,BarFG.transform.localScale.y, BarFG.transform.localScale.z);
        BarFG.transform.localPosition= new Vector3(-val, BarFG.transform.localPosition.y, BarFG.transform.localPosition.z);
    }

    void RefreshTray()
    {
        ProductA.sprite = ProductB.sprite = null;
        if (mProductsOrdered.Count > 1)
        {
            ProductB.sprite = GameManager.GM.GetProductSprite(mProductsOrdered[1]);
        }
        if (mProductsOrdered.Count > 0)
        {
            ProductA.sprite = GameManager.GM.GetProductSprite(mProductsOrdered[0]);
        }
    }

    public bool isActive()
    {
        if (mState == ClientState.Retreat)
        {
            return false;
        }
        return true;
    }
}