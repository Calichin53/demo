﻿public interface IAction {
    void Interact();
}