﻿using UnityEngine;

public class Node {

    Vector2 mIsometricPosition;
    public int gCost, hCost;
    public int isoX { get { return (int)mIsometricPosition.x; } }
    public int isoY { get { return (int)mIsometricPosition.y; } }
    public int fCost { get { return gCost + hCost; } }
    public bool Walkable;
    public Node parent;

    public Node(bool Walkeable, Vector2 IsoPosition)
    {
        Walkable = Walkeable;
        gCost = hCost = 0;
        mIsometricPosition = IsoPosition;
    }

    public int GetMovementCost(Node node)
    {
        int distanceX = Mathf.Abs(node.isoX - isoX);
        int distanceY = Mathf.Abs(node.isoY - isoY);

        if (distanceX > distanceY)
            return 14 * distanceY + 10 * (distanceX - distanceY);
        return 14 * distanceX + 10 * (distanceY - distanceX);
    }

    public int GetManhathanDistance(Node node)
    {
        return Mathf.Abs(node.isoX - isoX) + Mathf.Abs(node.isoY - isoY);
    }
}