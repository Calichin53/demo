﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager GM;
    public Machine[] machines;
    public Client[] clients;
    public Admin admin;
    public Sprite[] productSprites;
    public Sprite[] buttonSprites;
    public GameObject[] GesturePrefabs;

    IsometricGrid mGrid;
    PathFinding mAdminPathFinding;
    List<Vector2> mPositionStack;
    List<IAction> mInteractionStack;
    List<Node> mAdminPath;
    float currentSpawmingTime;

	void Awake () {
        GM = this;
        mPositionStack = new List<Vector2>();
        mInteractionStack = new List<IAction>();
        mGrid = new IsometricGrid(4, 3);
        mGrid.SetNotWalkable(1, 1);
        mGrid.SetNotWalkable(2, 1);
        admin.Position = mGrid.IsometricToWorldPosition(admin.isometricPosition);
        mAdminPathFinding = new PathFinding(mGrid);
        currentSpawmingTime = 0f;
        clients[0].HideAll();
        clients[1].HideAll();
        clients[2].HideAll();
    }
	
	void Update () {
        currentSpawmingTime += Time.deltaTime;
        if (currentSpawmingTime >= GameConstants.ClientSpawmingDelay)
            SpawmNewClient();
	}

    public List<ProductType> GenerateNewOrder(int MaxProducts)
    {
        int amountOfProducts = Random.Range(0, MaxProducts);
        List<ProductType> products = new List<ProductType>();
        for (int i = 0; i <= amountOfProducts; i++)
        {
            products.Add((ProductType)Random.Range(0,(int)ProductType.End));
        }
        return products;
    }

    public void AddAction(Vector2 isometricPosition, IAction entidadInteractuable)
    {//Agregar las referencias de la acción pendiente al Admin
        mPositionStack.Add(isometricPosition);
        mInteractionStack.Add(entidadInteractuable);
    }

    public void CalculateNextActionPath()
    {   //Si hay algun destino en el stack se calcula la ruta hasta el desde la posición actual del admin
        if (mPositionStack.Count > 0)
        {
            mAdminPath = mAdminPathFinding.GetPath(mGrid.GetNodeAtIsometric(admin.isometricPosition),mGrid.GetNodeAtIsometric(mPositionStack[0]));
            SetNewAdminDestiny();
        }
    }

    public void SetNewAdminDestiny()
    {//Cuando el Admin llega a un punto hay que evaluar si ya se encuentra en el punto final de la ruta, de no ser así se le asigna el siguiente punto (o nodo)
        if (mAdminPath.Count >= 2)
        {
            admin.SetNewDestiny(mGrid.IsometricToWorldPosition(mAdminPath[1]));
            admin.isometricPosition = new Vector2(mAdminPath[1].isoX, mAdminPath[1].isoY);
            mAdminPath.RemoveAt(0);
        }
        else if (mAdminPath.Count == 1 && mAdminPath[0] == mGrid.GetNodeAtIsometric(mPositionStack[0]))
        {//Hemos llegado al último nodo de la ruta
            mAdminPath.Clear();
            admin.Interact();
        }
        else
        {
            Debug.Log("ERROR: Esto no debería ocurrir nunca");
        }
    }

    public void ExecutesNextAction()
    {//Se intenta ejecutar la acción correspondiente y luego se retirar del stack tanto la posición alcanzada como la acción realizada
        mInteractionStack[0].Interact();
        mInteractionStack.RemoveAt(0);
        mPositionStack.RemoveAt(0);
    }

    public bool AdminPicksProduct(ProductType product)
    {//El admin intenta recoger un producto de la máquina (solo podrá si tiene las manos libres)
        return admin.TryToPickProduct(product);
    }

    public bool AdminDispatchProductTo(Client client)
    {
        if (client.TryReceiveProduct(admin.ProductInHand))
        {   //Si el cliente recibe el producto se vacía las manos del admin
            admin.ProductInHand = ProductType.End;
            return true;
        }
            return false;
    }

    public Sprite GetProductSprite(ProductType product)
    {
        return productSprites[(int)product];
    }

    public Sprite GetButtonSprite(ButtonType button)
    {
        return buttonSprites[(int)button];
    }

    public void DropGesture(GestureType gesture, Vector3 position)
    {
        Instantiate(GesturePrefabs[(int)gesture],position,Quaternion.identity);
    }

    void SpawmNewClient()
    {
        for (int i = 0; i < clients.Length; i++)
        {
            if (!clients[i].isActive())
            {
                clients[i].ResetClient();
                clients[i].ShowClient();
                currentSpawmingTime = 0;
                return;
            }
        }
    }
}