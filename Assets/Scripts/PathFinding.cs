﻿using System.Collections.Generic;

public class PathFinding {

    Node startNode, targetNode, currentNode;
    IsometricGrid mGrid;
    List<Node> openSet, closedSet, path;

	public PathFinding (IsometricGrid grid) {
        openSet = new List<Node>();
        closedSet = new List<Node>();
        path = new List<Node>();
        mGrid = grid;
    }

    void FindPath()
    {
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            currentNode = GetLowerFNode();
            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            if (currentNode == targetNode){
                RetracePath(startNode,targetNode);
                return;
            }

            foreach (Node neighbour in mGrid.GetNeighbours(currentNode))
            {
                if (!neighbour.Walkable || closedSet.Contains(neighbour))
                {
                    continue;
                }
                int newGCost = currentNode.gCost + currentNode.GetMovementCost(neighbour);
                if (newGCost < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newGCost;
                    neighbour.hCost = neighbour.GetManhathanDistance(targetNode);
                    neighbour.parent = currentNode;

                    if (!openSet.Contains(neighbour))
                        openSet.Add(neighbour);
                }
            }
        }
    }


    Node GetLowerFNode()
    {
        int minF = 10000;
        int minNodeIndex = 0;
        for (int i = 0; i < openSet.Count; i++)
        {
            if (openSet[i].fCost < minF)
            {
                minF = openSet[i].fCost;
                minNodeIndex = i;
            }
        }
        return openSet[minNodeIndex];
    }

    void RetracePath(Node start, Node end)
    {
        path = new List<Node>();
        Node current = end;
        while (current != start)
        {
            path.Add(current);
            current = current.parent;
        }
        path.Add(start);
        path.Reverse();
    }

    public List<Node> GetPath( Node start, Node target)
    {
        openSet.Clear();
        closedSet.Clear();
        path.Clear();
        startNode = start;
        targetNode = target;
        FindPath();
        return path;
    }
}